﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot
{
    class MyResponse
    {
        public bool Ok { get; private set; }
        public string ResultUri { get; private set; }
        public HttpWebResponse response { get; private set; }
        private HttpStatusCode resultCode;

        public MyResponse(string uri)
        {
            //Console.WriteLine("Response to "+uri);

            if (string.IsNullOrEmpty(uri))
            {
                Ok = false;
                
                return;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AllowAutoRedirect = true;
            request.MaximumAutomaticRedirections = 2;
            request.Timeout = 20000;
            response = null;
            try
            {
                response = request.GetResponse() as HttpWebResponse;
                resultCode = response.StatusCode;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    response.Close();
                    response = null;
                }
            }
            catch (Exception ex)
            {
                response = null;
            }

            Ok = response != null;
            ResultUri = Ok ? response.ResponseUri.AbsoluteUri : uri;

            //Console.WriteLine(Ok ? "ok" : "fail " + resultCode);
        }
    }
}
