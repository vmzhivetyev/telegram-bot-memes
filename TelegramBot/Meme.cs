﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Types.InlineQueryResults;

namespace TelegramBot
{
    class Meme
    {
        public bool Ok { get; private set; } = false;
        public bool IsVideo { get; private set; } = false;

        public string Uri { get; private set; } = "Ошибочка вышла, мемаса не будет D:";
        public string MediaUri { get; private set; } = "";
        private string VideoUri { get;  set; } = "";
        private string ImageUri { get;  set; } = "";
        public string Title { get; private set; } = "Ошибочка вышла, мемаса не будет D:";
        public MyResponse MediaResp { get; private set; }

        public const string RandomUri = @"http://9gag.com/random";
        private int v;

        public Meme() : this(RandomUri) { }
        public Meme(string uri)
        {
            var r = new MyResponse(uri);
            //r.response.Close();
            if (r.Ok)
            {
                Uri = r.ResultUri;
                Console.WriteLine(Uri);
                //try
                {
                    //WebClient x = new WebClient();
                    //string html = x.DownloadString(Uri);

                    Stream receiveStream = r.response.GetResponseStream();
                    StreamReader readStream = null;

                    if (r.response.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.GetEncoding(r.response.CharacterSet));
                    }

                    string html = readStream.ReadToEnd();

                    r.response.Close();
                    readStream.Close();

                    Title = Regex.Match(html, @"<title.*\>\s*(?<Title>.*) - 9GAG<\/title\>", RegexOptions.IgnoreCase).Groups["Title"].Value;
                    Title = "> " + WebUtility.HtmlDecode(Title);
                    ImageUri = Regex.Match(html, "class=\"badge-item-img\"\\s*src=\"(?<Title>.*?)\"", RegexOptions.IgnoreCase).Groups["Title"].Value;
                    VideoUri = Regex.Match(html, "<source src=\"(?<Title>http:\\/\\/img-9gag-fun\\.9cache\\.com\\/photo\\/.*?_460sv\\.mp4)\"", RegexOptions.IgnoreCase).Groups["Title"].Value;
                }
                //catch { }

                var vid = VideoUri != "";
                IsVideo = vid;
                MediaUri = vid ? VideoUri : ImageUri;
                MediaResp = new MyResponse(MediaUri);
            }
            Ok = r.Ok && MediaResp.response != null;
        }

        public static List<InlineQueryResult> GetInlineQueryResults(int count)
        {
            List<InlineQueryResult> list = new List<InlineQueryResult>();
            var tasks = new List<Task>();
            for (int i = 0; i < count; i++)
            {
                var t = new Task(() => AddMemeToQueryResultsList(ref list, i));
                tasks.Add(t);
                t.Start();
            }

            while (tasks.Any(x => x.Status == TaskStatus.Running))
            { }

            for(int i = 0; i < list.Count(); i++)
            {
                list[i].Id = i.ToString();
            }

            return list;
        }

        public static void AddMemeToQueryResultsList(ref List<InlineQueryResult> list, int i)
        {
            var m = new Meme();
            if (!m.Ok) return;
            m.MediaResp.response.Close();

            if (m.IsVideo)
                list.Add(new InlineQueryResultMpeg4Gif() { Url = m.MediaUri, Caption = m.Title, Title = m.Title, ThumbUrl = m.MediaUri });
            else
                list.Add(new InlineQueryResultPhoto()    { Url = m.MediaUri, Caption = m.Title, Title = m.Title, ThumbUrl = m.MediaUri });
        }
    }
}
