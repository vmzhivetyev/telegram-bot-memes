﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.InputMessageContents;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBot
{
    class Bot : TelegramBotClient
    {
        class Config
        {
            public string token;
        }

        const string configFile = "config.json";

        private static string ReadToken()
        {
            return JsonConvert.DeserializeObject<Config>(System.IO.File.ReadAllText(configFile)).token;
        }

        public Bot() : base(ReadToken())
        {
            new Task(() => RunBot()).Start();
        }

        private async void ProceedMsg(long _chatId, string _text, Message message = null)
        {
            var id = message != null ? message.Chat.Id : _chatId;
            var text = message != null ? message?.Text : _text;

            var from = message?.From;
            var photos = message?.Photo;

            if(from != null)
                Console.WriteLine(
                    "Msg from {0} {1} ({2}) at {4}: {3}",
                    from.FirstName,
                    from.LastName,
                    from.Username,
                    text,
                    message.Date);

            if (text == "/start")
            {
                var kb = new InlineKeyboardMarkup(new InlineKeyboardButton[]
                    {
                        new InlineKeyboardButton("Give me meme!", "/meme")
                    });

                //var kb = new ReplyKeyboardMarkup(new KeyboardButton[]
                //    {
                //        new KeyboardButton("/meme")
                //    }, true, false);

                await SendTextMessageAsync(id, "Привет! Я бот, кое-что умею..", false, false, 0, kb);

                return;
            }

            if (text == "/meme")
            {
                var m = new Meme();//@"http://9gag.com/gag/aqLZ6qZ");
                if (!m.Ok)
                {
                    await SendTextMessageAsync(id, m.Title);

                    return;
                }

                //Console.WriteLine(m.IsVideo + " " + m.Uri);

                using (var stream = m.MediaResp.response.GetResponseStream())
                {
                    FileToSend f = new FileToSend(m.MediaUri, stream);
                    try
                    {
                        if (m.IsVideo)
                            await SendVideoAsync(id, f, 0, m.Title);
                        else
                            await SendPhotoAsync(id, f, m.Title);
                        Console.WriteLine(m.Uri+ " sent");
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(m.Uri + " error: " + e.Message);
                    }
                    m.MediaResp.response.Close();
                }

                return;
            }
        }

        private async void RunBot()
        {
            var me = GetMeAsync().Result;
            if (me == null)
            {
                Console.WriteLine("GetMe() Error. Do you forget to add your AccessToken to " + configFile + "?");
                Console.ReadLine();
                Environment.Exit(0);
                return;
            }
            Console.WriteLine("{0} (@{1}) connected!", me.FirstName, me.Username);
            
            int offset = 0;
            while (true)
            {

                var updates = GetUpdatesAsync(offset).Result;
                if (updates != null)
                {
                    foreach (var update in updates)
                    {
                        offset = update.Id + 1;

                        if (update.Type == Telegram.Bot.Types.Enums.UpdateType.MessageUpdate)
                        {
                            new Task(() => ProceedMsg(0,"",update.Message)).Start();
                        }

                        if(update.Type == Telegram.Bot.Types.Enums.UpdateType.CallbackQueryUpdate)
                        {
                            new Task(() => ProceedMsg(update.CallbackQuery.From.Id, update.CallbackQuery.Data)).Start();
                        }

                        if (update.Type == Telegram.Bot.Types.Enums.UpdateType.InlineQueryUpdate)
                        {
                            var results = Meme.GetInlineQueryResults(4);
                            
                            try
                            {
                                await AnswerInlineQueryAsync(update.InlineQuery.Id, results.ToArray(), 0, true);
                                Console.WriteLine("Inline sent");
                            }
                            catch(Exception e)
                            {
                                Console.WriteLine("Inline query timed out");
                            }
                        }

                        //var m = await SendTextMessageAsync(id, DateTime.Now.ToLongTimeString());

                        #region commented
                        /*if (photos != null)
                        {
                            var webClient = new WebClient();
                            foreach (var photo in photos)
                            {
                                Console.WriteLine("  New image arrived: size {1}x{2} px, {3} bytes, id: {0}", photo.FileId, photo.Height, photo.Width, photo.FileSize);
                                var file = bot.MakeRequestAsync(new GetFile(photo.FileId)).Result;
                                var tempFileName = System.IO.Path.GetTempFileName();
                                webClient.DownloadFile(file.FileDownloadUrl, tempFileName);
                                Console.WriteLine("    Saved to {0}", tempFileName);
                            }
                        }

                        if (string.IsNullOrEmpty(text))
                        {
                            continue;
                        }
                        if (text == "/photo")
                        {
                            if (uploadedPhotoId == null)
                            {
                                var reqAction = new SendChatAction(update.Message.Chat.Id, "upload_photo");
                                bot.MakeRequestAsync(reqAction).Wait();
                                System.Threading.Thread.Sleep(500);
                                using (var photoData = Assembly.GetExecutingAssembly().GetManifestResourceStream("TelegramBotDemo.t_logo.png"))
                                {
                                    var req = new SendPhoto(update.Message.Chat.Id, new FileToSend(photoData, "Telegram_logo.png"))
                                    {
                                        Caption = "Telegram_logo.png"
                                    };
                                    var msg = bot.MakeRequestAsync(req).Result;
                                    uploadedPhotoId = msg.Photo.Last().FileId;
                                }
                            }
                            else
                            {
                                var req = new SendPhoto(update.Message.Chat.Id, new FileToSend(uploadedPhotoId))
                                {
                                    Caption = "Resending photo id=" + uploadedPhotoId
                                };
                                bot.MakeRequestAsync(req).Wait();
                            }
                            continue;
                        }
                        if (text == "/doc")
                        {
                            if (uploadedDocumentId == null)
                            {
                                var reqAction = new SendChatAction(update.Message.Chat.Id, "upload_document");
                                bot.MakeRequestAsync(reqAction).Wait();
                                System.Threading.Thread.Sleep(500);
                                using (var docData = Assembly.GetExecutingAssembly().GetManifestResourceStream("TelegramBotDemo.Telegram_Bot_API.htm"))
                                {
                                    var req = new SendDocument(update.Message.Chat.Id, new FileToSend(docData, "Telegram_Bot_API.htm"));
                                    var msg = bot.MakeRequestAsync(req).Result;
                                    uploadedDocumentId = msg.Document.FileId;
                                }
                            }
                            else
                            {
                                var req = new SendDocument(update.Message.Chat.Id, new FileToSend(uploadedDocumentId));
                                bot.MakeRequestAsync(req).Wait();
                            }
                            continue;
                        }
                        if (text == "/docutf8")
                        {
                            var reqAction = new SendChatAction(update.Message.Chat.Id, "upload_document");
                            bot.MakeRequestAsync(reqAction).Wait();
                            System.Threading.Thread.Sleep(500);
                            using (var docData = Assembly.GetExecutingAssembly().GetManifestResourceStream("TelegramBotDemo.Пример UTF8 filename.txt"))
                            {
                                var req = new SendDocument(update.Message.Chat.Id, new FileToSend(docData, "Пример UTF8 filename.txt"));
                                var msg = bot.MakeRequestAsync(req).Result;
                                uploadedDocumentId = msg.Document.FileId;
                            }
                            continue;
                        }
                        if (text == "/help")
                        {
                            var keyb = new ReplyKeyboardMarkup()
                            {
                                Keyboard = new[] { new[] { "/photo", "/doc", "/docutf8" }, new[] { "/help" } },
                                OneTimeKeyboard = true,
                                ResizeKeyboard = true
                            };
                            var reqAction = new SendMessage(update.Message.Chat.Id, "Here is all my commands") { ReplyMarkup = keyb };
                            bot.MakeRequestAsync(reqAction).Wait();
                            continue;
                        }
                        if (update.Message.Text.Length % 2 == 0)
                        {
                            bot.MakeRequestAsync(new SendMessage(
                                update.Message.Chat.Id,
                                "You wrote *" + update.Message.Text.Length + " characters*")
                            {
                                ParseMode = SendMessage.ParseModeEnum.Markdown
                            }).Wait();
                        }
                        else
                        {
                            bot.MakeRequestAsync(new ForwardMessage(update.Message.Chat.Id, update.Message.Chat.Id, update.Message.MessageId)).Wait();
                        }*/
                        #endregion
                    }
                }
            }
        }
    }
}
