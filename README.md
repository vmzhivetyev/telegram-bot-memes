## Telegram Memes Bot
- Sending you memes from 9GAG
	
## Usage
- Find @ekojBot in Telegram or follow the link [http://telegram.me/ekojBot](Link)
- Click "Start" button in Telegram
- Send him "/meme" if you want one ;)
- Also it can offer you a few random pics (or gifs) in any chat you want, just type "@ekojBot <something>" wait a second and see some magic

## Config
- "config.json" must be in directory where binary file is

### config.json
```json
{"token":"<bot token>"}
```